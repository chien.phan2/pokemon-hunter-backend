import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  BallTransaction,
  BallTransactionSchema,
} from 'src/schema/ball-transaction.schema';
import { InventoryModule } from '../inventory/inventory.module';
import { BallTransactionController } from './ball-transaction.controller';
import { BallTransactionService } from './ball-transaction.service';
import { BallModule } from '../ball/ball.module';
import { EventsModule } from 'src/events/events.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: BallTransaction.name, schema: BallTransactionSchema },
    ]),
    InventoryModule,
    BallModule,
    EventsModule,
  ],
  controllers: [BallTransactionController],
  providers: [BallTransactionService],
  exports: [BallTransactionService],
})
export class BallTransactionModule {}
