import {
  Body,
  Controller,
  HttpCode,
  Logger,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { BallTransactionService } from './ball-transaction.service';

@ApiTags('Ball Transaction')
@Controller('ball-transaction')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class BallTransactionController {
  constructor(
    private readonly ballTransactionService: BallTransactionService,
  ) {}

  @Post('/ipn')
  @HttpCode(204)
  ipnNoti(@Body() body: any) {
    console.log('Receive ball IPN:', body);
    Logger.log('Receive ball IPN:', body);
    return this.ballTransactionService.handleIpnNoti(body);
  }
}
