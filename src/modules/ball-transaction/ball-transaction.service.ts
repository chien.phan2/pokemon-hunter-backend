import {
  BadRequestException,
  Inject,
  Injectable,
  Logger,
  forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import fetch from 'node-fetch';
import {
  BallTransaction,
  BallTransactionDocument,
} from 'src/schema/ball-transaction.schema';
import genBase64 from 'src/utils/genBase64String';
import genDisbursementMethod from 'src/utils/genDisbursmentMethod';
import genSignature from 'src/utils/genSignature';
import { BallService } from '../ball/ball.service';
import { InventoryService } from '../inventory/inventory.service';
import { MomoIpn } from './dto/momo-ipn.dto';
import { EventsGateway } from 'src/events/events.gateway';

@Injectable()
export class BallTransactionService {
  constructor(
    @InjectModel(BallTransaction.name)
    private readonly ballTransactionModel: Model<BallTransactionDocument>,
    private readonly inventoryService: InventoryService,
    @Inject(forwardRef(() => BallService))
    private readonly ballService: BallService,
    private readonly eventsGateway: EventsGateway,
    @InjectConnection() private connection: Connection,
  ) {}

  async createTransaction(
    payload: {
      creator: string;
      product: string;
      quantity: number;
      value: number;
    },
    productName: string,
  ): Promise<any> {
    console.log('====> payload:', payload);
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const newTransactions = await this.ballTransactionModel.create(
        [
          {
            creator: payload.creator,
            product: payload.product,
            status: 'pending',
            quantity: payload.quantity,
            value: payload.value,
            type: 'buy',
            signature: '',
            paymentInfo: {},
          },
        ],
        {
          session,
        },
      );
      const newTransaction = newTransactions[0];

      const [signature, paymentInfo] = await this.createMomoTransaction(
        newTransaction,
        productName,
        session,
      );

      newTransaction.paymentInfo = paymentInfo;
      newTransaction.signature = signature;

      await newTransaction.save({ session });

      this.setTransactionRequestFailedAfterTime(
        newTransaction._id?.toString(),
        70000,
      );

      await session.commitTransaction();
      session.endSession();

      return {
        data: paymentInfo,
      };
    } catch (error) {
      console.log('createTransaction: ', error);
      Logger.error('Create ball transaction failed', error);

      await session.abortTransaction();
      session.endSession();

      throw new BadRequestException(error);
    }
  }

  async createMomoTransaction(
    transaction: BallTransaction,
    productName: string,
    session: any,
  ): Promise<any> {
    console.log('====> createMomoTransaction: transaction:', transaction);
    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_RECEIVE,
        partnerName: process.env.PARTNER_NAME,
        storeId: 'MomoTestStore',
        requestType: 'captureWallet',
        orderId: transaction._id.toString(),
        orderInfo: `Thanh toán ${productName}`,
        amount: transaction.value,
        lang: 'vi',
        redirectUrl: `momo://?refId=${
          process.env.MINI_APP_ID
        }&transaction=${transaction._id.toString()}&appId=${
          process.env.MINI_APP_ID
        }&deeplink=true`,
        requestId: transaction._id.toString(),
        extraData: transaction.product,
        autoCapture: false,
        signature: '',
      };

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&extraData=${data.extraData}&ipnUrl=${data.ipnUrl}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&redirectUrl=${data.redirectUrl}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_TRANSACTION, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });

      const responseData = await response.json();

      if (responseData['resultCode'] != 0) {
        await this.ballTransactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            status: 'error',
            paymentInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        throw new BadRequestException('Create ball transaction failed');
      }

      // Phuc vu dev
      // const userId = JSON.stringify(transaction.creator).replace(/"/g, '');
      // const createPromises = [];
      // const productList = await this.ballService.getAllBalls('product');
      // const productIdslist = productList?.map((product) =>
      //   product?._id?.toString(),
      // );
      // for (let i = 0; i < 3; i++) {
      //   createPromises.push(
      //     this.inventoryService.createInventoryItem(
      //       { product: productIdslist[i] },
      //       userId,
      //     ),
      //   );
      // }
      // await Promise.all(createPromises);
      // const inventoryRemain =
      //   await this.inventoryService.getInventoryItemByUserID(userId);
      // setTimeout(() => {
      //   this.eventsGateway.onChangePokeball({
      //     userID: transaction.creator._id?.toString(),
      //     inventory: inventoryRemain,
      //   });
      // }, 3000);
      // Remove tren prod

      return [signature, responseData];
    } catch (error) {
      console.log('createMomoTransaction: ', error);
      await this.ballTransactionModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { status: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      throw new BadRequestException(error);
    }
  }

  setTransactionRequestFailedAfterTime(transactionID: string, timeout: number) {
    setTimeout(async () => {
      const session = await this.connection.startSession();
      session.startTransaction();

      try {
        const transaction = await this.ballTransactionModel.findOne(
          {
            _id: transactionID,
            status: 'pending',
          },
          null,
          { session },
        );
        if (transaction) {
          await transaction.updateOne(
            { status: 'failed', updatedAt: new Date().toLocaleString() },
            { session },
          );
        }
        await session.commitTransaction();
        await session.endSession();
      } catch (error) {
        console.log('setTransactionRequestFailedAfterTime: ', error);
        Logger.error(error);
        Logger.warn(
          'ERROR: Can not set energy transaction status for',
          transactionID,
        );
        await session.abortTransaction();
        await session.endSession();
      }
    }, timeout);
  }

  async handleIpnNoti(data: MomoIpn) {
    console.log('====> handleIpnNoti data:', data);
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const transactionID = data.orderId;
      const productID = data.extraData;

      const transaction = await this.ballTransactionModel
        .findOne({
          _id: transactionID,
          product: productID,
          value: data.amount,
        })
        .session(session)
        .populate('creator')
        .populate('product');

      if (transaction) {
        if (data.resultCode == 9000) {
          if (transaction.status == 'pending') {
            this.sendConfirmToMomo(transaction);
          } else this.sendCancelToMomo(transaction);
        } else if (data.resultCode != 0 && data.resultCode != 9000) {
          await transaction.updateOne(
            {
              status: 'failed',
              paymentInfo: { ...transaction.paymentInfo, ...data },
            },
            { session },
          );

          // setTimeout(() => {
          //   this.eventsGateway.onEnergyTransaction({
          //     productID: productID,
          //     status: 'fail',
          //     transactionID: data.orderId,
          //   });
          // }, 10000);

          await session.commitTransaction();
        }
      }
    } catch (error) {
      const productID = data.extraData;
      // setTimeout(() => {
      //   this.eventsGateway.onEnergyTransaction({
      //     productID: productID,
      //     status: 'fail',
      //     transactionID: data.orderId,
      //   });
      // }, 10000);
      console.log('handleIpnNoti: ', error);
      Logger.log('Update energy transaction status failed', error);
    } finally {
      await session.endSession();

      return {
        data: 'success',
      };
    }
  }

  async sendConfirmToMomo(transaction: BallTransaction) {
    console.log('====> sendConfirmToMomo ===> transaction:', transaction);
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'capture',
        amount: transaction.value,
        lang: 'vi',
        description: `Thanh toán ${transaction?.product?.name || 'Pokeball'}`,
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      console.log('====> sendConfirmToMomo: response:', response);
      const responseData = await response.json();
      Logger.log('Receive response', responseData);

      const transactionID = responseData.orderId;
      const productID = String(transaction.product._id);

      if (responseData.resultCode == 0) {
        await this.ballTransactionModel.findOneAndUpdate(
          { _id: transactionID, status: 'pending' },
          {
            status: 'success',
            statusRequestPay: 'pending',
            paymentInfo: { ...transaction.paymentInfo, ...responseData },
          },
          session,
        );

        const cashOut = await this.cashOut(
          transaction?.value,
          transaction?._id?.toString(),
        );
        await this.ballTransactionModel.findOneAndUpdate(
          { _id: transactionID },
          {
            cashOutInfo: { ...cashOut },
          },
          session,
        );

        await session.commitTransaction();

        // const quantity = transaction?.quantity || 0;
        const createPromises = [];
        const productList = await this.ballService.getAllBalls('product');
        const productIdslist = productList?.map((product) =>
          product?._id?.toString(),
        );
        for (let i = 0; i < 3; i++) {
          createPromises.push(
            this.inventoryService.createInventoryItem(
              { product: productIdslist[i] },
              transaction.creator._id?.toString(),
            ),
          );
        }
        await Promise.all(createPromises);

        const inventoryRemain =
          await this.inventoryService.getInventoryItemByUserID(
            transaction.creator._id?.toString(),
          );

        setTimeout(() => {
          this.eventsGateway.onChangePokeball({
            userID: transaction.creator._id?.toString(),
            inventory: inventoryRemain,
          });
        }, 7000);

        // setTimeout(() => {
        //   this.eventsGateway.onTransaction({
        //     ticketID: productID,
        //     status: 'success',
        //     transactionID: responseData.orderId,
        //   });
        // }, 10000);
      }
    } catch (err) {
      Logger.error('Send confirm to momo failed', err);
      throw new BadRequestException(err);
    }
  }

  async sendCancelToMomo(transaction: BallTransaction) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'cancel',
        amount: transaction.value,
        lang: 'vi',
        description: `Thanh toán ${transaction?.product?.name || 'Pokeball'}`,
        signature: '',
      };

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData = await response.json();
      Logger.log('Receive sendCancelToMomo response: ', responseData);

      const productID = String(transaction.product);

      // if (responseData.resultCode == 0) {
      //         // socket here
      //         setTimeout(() => {
      //           this.eventsGateway.onEnergyTransaction({
      //             productID: productID,
      //             status: 'failed',
      //             transactionID: responseData.orderId,
      //           });
      //         }, 10000);
      //       }

      await session.commitTransaction();
    } catch (error) {
      Logger.error('Send cancel to momo failed', error);
      throw new BadRequestException(error);
    }
  }

  async cashOut(amount: number, transactionId) {
    console.log('====> cashOut: transactionId:', transactionId);
    console.log('====> cashOut: amount:', amount);
    const controller = new AbortController();
    const setTimeoutId = setTimeout(() => controller.abort(), 30000);

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_SEND,
        orderId: 'cashout_' + transactionId,
        orderInfo: 'Chuyển tiền mua rổ',
        extraData: '',
        lang: 'vi',
        amount: amount,
        walletId: '0912400482',
        requestId: 'cashout_' + transactionId,
        requestType: 'disburseToWallet',
        disbursementMethod: '',
        signature: '',
      };

      data.extraData = genBase64(`{"id": "${'cashout_' + transactionId}"}`);

      const disbursementData = {
        partnerCode: process.env.PARTNER_CODE,
        partnerRefId: 'cashout_' + transactionId,
        partnerTransId: 'cashout_' + transactionId,
        amount: amount,
        description: 'Chuyển tiền mua rổ',
        walletId: '0912400482',
      };

      data.disbursementMethod = genDisbursementMethod(
        JSON.stringify(disbursementData),
      );

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&disbursementMethod=${data.disbursementMethod}&extraData=${data.extraData}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);
      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_REQUEST, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
        signal: controller.signal,
      });
      console.log('====> cashOut ==> response:', response);
      const responseData = await response.json();

      if (setTimeoutId) clearTimeout(setTimeoutId);

      console.log(responseData);
      return responseData;
    } catch (error) {
      console.log('cashOut', error);
      throw new BadRequestException(error);
    }
  }
}
