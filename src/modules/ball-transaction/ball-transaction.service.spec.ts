import { Test, TestingModule } from '@nestjs/testing';
import { BallTransactionService } from './ball-transaction.service';

describe('BallTransactionService', () => {
  let service: BallTransactionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BallTransactionService],
    }).compile();

    service = module.get<BallTransactionService>(BallTransactionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
