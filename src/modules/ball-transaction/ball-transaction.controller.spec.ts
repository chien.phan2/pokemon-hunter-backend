import { Test, TestingModule } from '@nestjs/testing';
import { BallTransactionController } from './ball-transaction.controller';

describe('BallTransactionController', () => {
  let controller: BallTransactionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BallTransactionController],
    }).compile();

    controller = module.get<BallTransactionController>(BallTransactionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
