import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Ball, BallSchema } from 'src/schema/ball.schema';
import { BallTransactionModule } from '../ball-transaction/ball-transaction.module';
import { UserModule } from '../user/user.module';
import { BallController } from './ball.controller';
import { BallService } from './ball.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Ball.name, schema: BallSchema }]),
    UserModule,
    forwardRef(() => BallTransactionModule),
  ],
  controllers: [BallController],
  providers: [BallService],
  exports: [BallService],
})
export class BallModule {}
