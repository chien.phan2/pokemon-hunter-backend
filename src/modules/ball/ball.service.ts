import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Ball, BallDocument } from 'src/schema/ball.schema';
import { Model } from 'mongoose';
import { CreateBallDto } from './dto/create-ball.dto.ts';
import { UpdateBallDto } from './dto/update-ball.dto.ts.js';
import { CreateBallTransactionDto } from '../ball-transaction/dto/create-ball-transaction.dto.ts.js';
import { UserService } from '../user/user.service.js';
import { BallTransactionService } from '../ball-transaction/ball-transaction.service.js';

@Injectable()
export class BallService {
  constructor(
    @InjectModel(Ball.name) private readonly ballModel: Model<BallDocument>,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    @Inject(forwardRef(() => BallTransactionService))
    private readonly ballTransactionService: BallTransactionService,
  ) {}

  async createBall(payload: CreateBallDto): Promise<Ball> {
    try {
      const newBall = new this.ballModel(payload);
      return newBall.save();
    } catch (error) {
      console.log('createBall: ', error);
      throw new BadRequestException(error);
    }
  }

  async updateBall(payload: UpdateBallDto, id: string): Promise<Ball> {
    try {
      const updatedBall = await this.ballModel.findByIdAndUpdate(
        id,
        { ...payload, updatedAt: new Date() },
        { new: true },
      );
      return updatedBall;
    } catch (error) {
      console.log('updateBall: ', error);
      throw new BadRequestException(error);
    }
  }

  async getAllBalls(type: string): Promise<Ball[]> {
    try {
      const balls = await this.ballModel
        .find({
          type: type,
        })
        .sort({
          price: 1,
        });
      return balls;
    } catch (error) {
      console.log('getAllBalls: ', error);
      throw new BadRequestException(error);
    }
  }

  async getBallById(id: string): Promise<Ball> {
    try {
      const ball = await this.ballModel.findById(id).lean();
      return ball;
    } catch (error) {
      console.log('getBallById: ', error);
      throw new BadRequestException(error);
    }
  }

  async deleteBallById(id: string): Promise<Ball> {
    try {
      const ball = await this.ballModel.findByIdAndDelete(id);
      return ball;
    } catch (error) {
      console.log('deleteBallById: ', error);
      throw new BadRequestException(error);
    }
  }

  async createRequestBuyBall(
    userID: string,
    payload: CreateBallTransactionDto,
  ): Promise<any> {
    if (payload?.quantity < 0) {
      throw new BadRequestException('Quantity must be greater than 0');
    }

    try {
      const user = await this.userService.getUserInfo(userID);
      const ball = await this.getBallById(payload?.productId);
      const data = {
        product: ball?._id?.toString(),
        quantity: payload?.quantity,
        value: (ball?.price || 0) * (payload?.quantity || 0),
        creator: user?._id.toString(),
      };

      const productName = ball?.name || 'Ball';

      const transactionData =
        await this.ballTransactionService.createTransaction(data, productName);

      return transactionData;
    } catch (error) {
      console.log('requestBuyBall: ', error);
      throw new BadRequestException(error);
    }
  }
}
