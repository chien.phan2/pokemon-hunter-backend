import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { CreateBallTransactionDto } from '../ball-transaction/dto/create-ball-transaction.dto.ts';
import { BallService } from './ball.service';
import { CreateBallDto } from './dto/create-ball.dto.ts';
import { UpdateBallDto } from './dto/update-ball.dto.ts';

@ApiTags('Ball')
@Controller('ball')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class BallController {
  constructor(private readonly ballService: BallService) {}

  @Post('/create')
  async createBall(@Body() payload: CreateBallDto) {
    return this.ballService.createBall(payload);
  }

  @Patch('/update/:id')
  async updateBall(@Body() payload: UpdateBallDto, @Param('id') id: string) {
    return this.ballService.updateBall(payload, id);
  }

  @Get('/get-all')
  async getAllBalls(@Query('type') type: string) {
    return this.ballService.getAllBalls(type);
  }

  @Get('/:id')
  async getBallById(@Param('id') id: string) {
    return this.ballService.getBallById(id);
  }

  @Post('/request-buy')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  async requestBuyBall(
    @Body() payload: CreateBallTransactionDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.ballService.createRequestBuyBall(userID, payload);
  }
}
