import { Test, TestingModule } from '@nestjs/testing';
import { CatchHistoryService } from './catch-history.service';

describe('CatchHistoryService', () => {
  let service: CatchHistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CatchHistoryService],
    }).compile();

    service = module.get<CatchHistoryService>(CatchHistoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
