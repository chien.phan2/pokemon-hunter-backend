import { Test, TestingModule } from '@nestjs/testing';
import { CatchHistoryController } from './catch-history.controller';

describe('CatchHistoryController', () => {
  let controller: CatchHistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CatchHistoryController],
    }).compile();

    controller = module.get<CatchHistoryController>(CatchHistoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
