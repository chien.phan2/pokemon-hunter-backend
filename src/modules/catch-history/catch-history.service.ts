import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import axios from 'axios';
import { Connection, Model } from 'mongoose';
import { EventsGateway } from 'src/events/events.gateway';
import {
  CatchHistory,
  CatchHistoryDocument,
} from 'src/schema/catch-history.schema';
import { BallService } from '../ball/ball.service';
import { InventoryService } from '../inventory/inventory.service';
import { PokemonService } from '../pokemon/pokemon.service';
import { UserService } from '../user/user.service';
import { CreateCatchHistoryDto } from './dto/create-catch-history.dto';

@Injectable()
export class CatchHistoryService {
  constructor(
    @InjectModel(CatchHistory.name)
    private catchHistoryModel: Model<CatchHistoryDocument>,
    private readonly inventoryService: InventoryService,
    private readonly userServices: UserService,
    private readonly pokemonService: PokemonService,
    private readonly ballService: BallService,
    private readonly eventsGateway: EventsGateway,
    @InjectConnection() private connection: Connection,
  ) {}

  async getReconigition(
    image: string,
    fortune: number,
    // transactionID: string
  ) {
    // const formData = new FormData();
    // formData.append('file', file?.buffer, file?.originalname);
    // formData.append('transactionId', transactionID);

    // console.log('===> getReconigition ~ formData:', formData);

    const data = {
      msgType: 'FIND_FORTUNE_MSG',
      fortune: fortune,
      faceBase64: image,
    };

    try {
      const response = await axios.post(
        process.env.URL_FACE_MATCHING,
        JSON.stringify(data),
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );

      return response.data;
    } catch (error) {
      console.log('===> getReconigition ==> error:', error);
      throw new BadRequestException(error);
    }
  }

  async createCatchHistory(userID: string, payload: CreateCatchHistoryDto) {
    // console.log('=====> payload:', payload);
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      // Check current pokemon equal to pokemon in payload
      // if not => throw error
      const currentPokemon = await this.pokemonService.getCurrentPokemon();
      // console.log('====> currentPokemon:', currentPokemon);

      const existPokemon = currentPokemon.find(
        (item) => item?._id?.toString() === payload.pokemon,
      );
      // console.log('====> existPokemon:', existPokemon);

      const existHistory = await this.checkExistCatchHistory(
        userID,
        payload.pokemon,
      );

      if (!existPokemon) {
        return {
          data: {
            status: 'error',
            reward: 0,
            message: 'Nhầm người rồi bạn ơi!',
          },
        };
      }

      if (existHistory) {
        return {
          data: {
            status: 'error',
            reward: 0,
            message: 'Duyên nợ kiếp này đã hết, hãy thử với Nam thần khác nhé!',
          },
        };
      }

      // Check have ball in inventory
      // if not => throw error
      // if have => create history
      // const existBall = await this.inventoryService.checkInventoryItemExist(
      //   userID,
      //   payload.product,
      // );

      // if (!existBall || existBall?.length === 0) {
      //   return {
      //     data: {
      //       status: 'error',
      //       reward: 0,
      //       message: 'Bạn không có rổ này trong kho',
      //     },
      //   };
      // }

      // Update inventory item to used
      // await this.inventoryService.setInventoryItemUsed(
      //   existBall[0]._id?.toString(),
      // );

      // Create history

      const newHistories = await this.catchHistoryModel.create(
        [
          {
            creator: userID,
            pokemon: payload.pokemon,
            product: payload.product,
            status: 'pending',
            reward: 0,
            rewardInfo: {},
          },
        ],
        {
          session,
        },
      );
      const newHistory = newHistories[0];

      // const currentBall = await this.ballService.getBallById(payload.product);

      // // Check rate to catch pokemon
      // const rate = currentBall?.rate || 0;
      // const random = Math.random() * 100;
      // if (random > rate) {
      //   newHistory.status = 'failed';
      // }

      // Call API to check face recognition
      // if true => update status to success
      // if false => update status to failed
      const fortune = Number(existPokemon?.fortune);
      if (!fortune) {
        throw new BadRequestException('Thông tin không hợp lệ!');
      }
      // const { resultCode } = await this.getReconigition(
      //   payload?.image,
      //   fortune,
      // );
      // console.log('===>>>>>>>> result:', resultCode);
      const resultCode = 0;

      if (resultCode === 0) {
        // Update history to success
        newHistory.status = 'success';

        // Call API to get reward
        // if success => update reward and rewardInfo
        // if failed => return

        const rewardData = await this.pokemonService.calculateBudgetPokemon(
          payload.pokemon,
        );
        console.log('=====> rewardData:', rewardData);
        const { reward, bonusReward, totalReward, status } = rewardData;

        if (status === 'failed') {
          // await this.inventoryService.restoreInventoryItem(
          //   existBall[0]._id?.toString(),
          // );

          newHistory.status = 'failed';
        }

        newHistory.reward = totalReward || 0;
        newHistory.rewardInfo = {
          reward: reward || 0,
          bonusReward: bonusReward || 0,
        };
      } else {
        newHistory.status = 'failed';
      }

      await newHistory.save({ session });

      await session.commitTransaction();
      session.endSession();

      // const inventoryRemain =
      //   await this.inventoryService.getInventoryItemByUserID(userID);

      // console.log('inventoryRemain: ', inventoryRemain);

      if (newHistory.status === 'success') {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const updateUserPoint = await this.userServices.updatePointOfUser(
          userID,
          newHistory.reward,
        );

        const rewardOfUser = await this.getHistoryByUserId(userID);
        const totalReward = rewardOfUser.totalReward || 0;
        setTimeout(() => {
          this.eventsGateway.onChangeReward({
            userID: userID,
            totalReward: totalReward + newHistory.reward,
          });
        }, 2000);
      }

      // setTimeout(() => {
      //   this.eventsGateway.onChangePokeball({
      //     userID: userID,
      //     inventory: inventoryRemain,
      //   });
      // }, 1000);

      console.log('response', {
        data: {
          status: newHistory.status,
          reward: newHistory.reward,
        },
      });

      return {
        data: {
          status: newHistory.status,
          reward: newHistory.reward,
          message:
            newHistory.status === 'success'
              ? 'Uống nhầm 1 ánh mắt, quà theo về tận nhà!'
              : // : resultCode === 718
                // ? 'Hãy thử lại với ảnh rõ hơn nhé!'
                // : resultCode === 739
                // ? 'Hãy thử lại với Nam thần khác nhé!'
                'Đã có lỗi xảy ra, vui lòng thử lại sau!',
        },
      };
    } catch (error) {
      console.log('createCatchHistory: ', error);
      await session.abortTransaction();
      session.endSession();

      throw new BadRequestException(error);
    }
  }

  async getHistoryByUserId(userID: string) {
    try {
      const histories = await this.catchHistoryModel
        .find({ creator: userID })
        .sort({ createdAt: -1 })
        .populate('pokemon')
        .populate('product')
        .exec();

      const totalReward = histories.reduce(
        (total, item) => total + item.reward,
        0,
      );

      return {
        totalReward: totalReward,
        histories: histories,
      };
    } catch (error) {
      console.log('getHistoryByUserId: ', error);
      throw new BadRequestException(error);
    }
  }

  async getDetailByHistoryId(id: string) {
    try {
      const history = await this.catchHistoryModel.findOne({
        _id: id,
      });
      return history;
    } catch (error) {
      console.log('getDetailByHistoryId: ', error);
      throw new BadRequestException(error);
    }
  }

  async getAllHistories() {
    try {
      const userList = await this.userServices.getAllUser();

      const history = await this.catchHistoryModel
        .aggregate([
          {
            $group: {
              _id: '$creator',
              totalCatch: { $sum: 1 },
              totalReward: { $sum: '$reward' },
            },
          },
          {
            $sort: { totalReward: -1 },
          },
        ])
        .exec();

      const data = history.map((item) => {
        const user = userList.find(
          (user) => user._id.toString() === item._id.toString(),
        );
        return {
          ...item,
          user: user,
        };
      });

      return data;
    } catch (error) {
      console.log('getHistoryAllUser: ', error);
      throw new BadRequestException(error);
    }
  }

  async checkExistCatchHistory(userID: string, pokemonID: string) {
    try {
      const histories = await this.catchHistoryModel.find({
        creator: userID,
        pokemon: pokemonID,
        status: 'success',
      });

      return histories?.length > 0;
    } catch (error) {
      console.log('checkExistCatchHistory: ', error);
      throw new BadRequestException(error);
    }
  }

  async countValueAllHistory() {
    try {
      const total = await this.catchHistoryModel.aggregate([
        {
          $match: {
            status: 'success',
          },
        },
        {
          $group: {
            _id: null,
            totalReward: { $sum: '$reward' },
          },
        },
      ]);

      return total?.[0]?.totalReward || 0;
    } catch (error) {
      console.log('countValueAllHistory: ', error);
      throw new BadRequestException(error);
    }
  }
}
