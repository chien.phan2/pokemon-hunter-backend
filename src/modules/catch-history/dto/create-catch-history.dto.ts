import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCatchHistoryDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  product: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  pokemon: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  image: string;
}
