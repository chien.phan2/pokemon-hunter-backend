import { ApiProperty } from '@nestjs/swagger';
import { Express } from 'express';

export class FileDto {
  @ApiProperty()
  file: Express.Multer.File;
}
