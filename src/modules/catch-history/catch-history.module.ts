import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  CatchHistory,
  CatchHistorySchema,
} from 'src/schema/catch-history.schema';
import { InventoryModule } from '../inventory/inventory.module';
import { UserModule } from '../user/user.module';
import { CatchHistoryController } from './catch-history.controller';
import { CatchHistoryService } from './catch-history.service';
import { PokemonModule } from '../pokemon/pokemon.module';
import { EventsModule } from 'src/events/events.module';
import { BallModule } from '../ball/ball.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: CatchHistory.name, schema: CatchHistorySchema },
    ]),
    InventoryModule,
    UserModule,
    PokemonModule,
    BallModule,
    EventsModule,
  ],
  exports: [CatchHistoryService],
  providers: [CatchHistoryService],
  controllers: [CatchHistoryController],
})
export class CatchHistoryModule {}
