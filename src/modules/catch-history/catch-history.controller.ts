import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { AuthGuard, PortalAuthGuard } from 'src/middleware/auth.middleware';
import { CatchHistoryService } from './catch-history.service';
import { CreateCatchHistoryDto } from './dto/create-catch-history.dto';

@ApiTags('History Catch Pokemon')
@Controller('catch-history')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class CatchHistoryController {
  constructor(private readonly catchHistoryService: CatchHistoryService) {}

  @Get('/portal/count-value')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async countValueAllHistory() {
    return this.catchHistoryService.countValueAllHistory();
  }

  @Post('/create')
  // @ApiConsumes('multipart/form-data')
  // @ApiBody({
  //   schema: {
  //     type: 'object',
  //     properties: {
  //       product: { type: 'string', nullable: false },
  //       pokemon: { type: 'string', nullable: false },
  //       file: {
  //         type: 'string',
  //         format: 'binary',
  //       },
  //     },
  //   },
  // })
  // @UseInterceptors(FileInterceptor('file'))
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  async createHistory(
    @Body() payload: CreateCatchHistoryDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.catchHistoryService.createCatchHistory(userID, payload);
  }

  @Get('/')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  async getMyHistory(@Req() req: Request) {
    const userID = req.userID;
    return this.catchHistoryService.getHistoryByUserId(userID);
  }

  @Get('/all')
  async getAllHistories() {
    return this.catchHistoryService.getAllHistories();
  }

  @Get('/detail/:id')
  async getMyHistoryByID(id: string) {
    return this.catchHistoryService.getDetailByHistoryId(id);
  }
}
