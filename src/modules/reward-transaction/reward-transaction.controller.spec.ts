import { Test, TestingModule } from '@nestjs/testing';
import { RewardTransactionController } from './reward-transaction.controller';

describe('RewardTransactionController', () => {
  let controller: RewardTransactionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RewardTransactionController],
    }).compile();

    controller = module.get<RewardTransactionController>(
      RewardTransactionController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
