import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import fetch from 'node-fetch';
import {
  RewardTransaction,
  RewardTransactionDocument,
} from 'src/schema/reward-transaction.schema';
import genBase64 from 'src/utils/genBase64String';
import genDisbursementMethod from 'src/utils/genDisbursmentMethod';
import genSignature from 'src/utils/genSignature';
import { MomoIpn } from '../ball-transaction/dto/momo-ipn.dto.js';
import { PokemonService } from '../pokemon/pokemon.service.js';
import { UserService } from '../user/user.service.js';
import { CreateRewardTransactionDto } from './dto/create-reward-transaction.dto.ts';

@Injectable()
export class RewardTransactionService {
  constructor(
    @InjectModel(RewardTransaction.name)
    private readonly rewardTransactionModel: Model<RewardTransactionDocument>,
    private readonly userService: UserService,
    private readonly pokemonService: PokemonService,
    @InjectConnection() private connection: Connection,
  ) {}

  async createRequestReward(
    payload: CreateRewardTransactionDto,
  ): Promise<RewardTransaction> {
    try {
      const user = await this.userService.getUserInfo(payload?.receiver);
      const pokemon = await this.pokemonService.getPokemonById(
        payload?.pokemon,
      );

      const data = {
        receiver: user?._id?.toString(),
        pokemon: pokemon?._id?.toString(),
        value: payload?.value || 0,
      };

      const pokemonName = pokemon?.name || 'Pokemon';

      const rewardTransactionData = this.createRewardTransaction(
        data,
        pokemonName,
      );

      return rewardTransactionData;
    } catch (error) {
      console.log('createRequestReward: ', error);
      throw new BadRequestException(error);
    }
  }

  async createRewardTransaction(
    payload: CreateRewardTransactionDto,
    productName: string,
  ): Promise<any> {
    console.log('----> RewardTransactionService ===> payload:', payload);
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const newRewardTransactions = await this.rewardTransactionModel.create(
        [
          {
            receiver: payload?.receiver,
            product: payload?.pokemon,
            status: 'pending',
            value: payload?.value,
            signature: '',
            paymentInfo: {},
            statusRequestPay: 'pending',
            requestPayInfo: {},
          },
        ],
        { session },
      );
      const newRewardTransaction = newRewardTransactions[0];

      const [signature, paymentInfo] = await this.createMomoRewardTransaction(
        newRewardTransaction,
        productName,
        session,
      );

      newRewardTransaction.paymentInfo = paymentInfo;
      newRewardTransaction.signature = signature;

      await newRewardTransaction.save({ session });

      this.setRewardTransactionRequestFailedAfterTime(
        newRewardTransaction._id?.toString(),
        70000,
      );

      await session.commitTransaction();
      session.endSession();

      return {
        data: paymentInfo,
      };
    } catch (error) {
      console.log('createRewardTransaction: ', error);
      Logger.error('Create ball transaction failed', error);

      await session.abortTransaction();
      session.endSession();

      throw new BadRequestException(error);
    }
  }

  async createMomoRewardTransaction(
    rewardTransaction: RewardTransaction,
    productName: string,
    session: any,
  ) {
    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_RECEIVE,
        partnerName: process.env.PARTNER_NAME,
        storeId: 'MomoTestStore',
        requestType: 'captureWallet',
        orderId: rewardTransaction._id.toString(),
        orderInfo: `Nhận tiền thưởng bắt ${productName}`,
        amount: rewardTransaction.value,
        lang: 'vi',
        redirectUrl: `momo://?refId=${
          process.env.ENVIRONMENT == 'pro'
            ? process.env.MINI_APP_ID
            : process.env.MINI_APP_ID
        }&transaction=${rewardTransaction._id.toString()}&appId=${
          process.env.MINI_APP_ID
        }&deeplink=true`,
        requestId: rewardTransaction._id.toString(),
        extraData: rewardTransaction.product,
        autoCapture: false,
        signature: '',
      };

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&extraData=${data.extraData}&ipnUrl=${data.ipnUrl}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&redirectUrl=${data.redirectUrl}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_TRANSACTION, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });

      const responseData = await response.json();
      Logger.log('Receive reward response', responseData);

      if (responseData['resultCode'] != 0) {
        await this.rewardTransactionModel.findOneAndUpdate(
          { _id: rewardTransaction._id.toString() },
          {
            status: 'error',
            paymentInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        throw new BadRequestException('Create reward transaction failed');
      }

      return [signature, responseData];
    } catch (error) {
      console.log('createMomoRewardTransaction: ', error);
      await this.rewardTransactionModel.findOneAndUpdate(
        { _id: rewardTransaction._id.toString() },
        { status: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      throw new BadRequestException(error);
    }
  }

  setRewardTransactionRequestFailedAfterTime(
    transactionID: string,
    timeout: number,
  ) {
    setTimeout(async () => {
      const session = await this.connection.startSession();
      session.startTransaction();

      try {
        const transaction = await this.rewardTransactionModel.findOne(
          {
            _id: transactionID,
            status: 'pending',
          },
          null,
          { session },
        );
        if (transaction) {
          await transaction.updateOne(
            { status: 'failed', updatedAt: new Date().toLocaleString() },
            { session },
          );
        }
        await session.commitTransaction();
        await session.endSession();
      } catch (err) {
        console.log('setRewardTransactionRequestFailedAfterTime: ', err);
        Logger.error(err);
        Logger.warn('ERROR: Can not set transaction status for', transactionID);
        await session.abortTransaction();
        await session.endSession();
      }
    }, timeout);
  }

  async handleIpnNoti(data: MomoIpn): Promise<any> {
    // console.log('====> handleIpnNoti =>> reward data:', data);
    // const session = await this.connection.startSession();
    // session.startTransaction();

    try {
      return {
        data,
      };

      // const transactionID = data.orderId;
      // const productID = data.extraData;

      // const rewardTransaction = await this.rewardTransactionModel
      //   .findOne({
      //     _id: transactionID,
      //     product: productID,
      //     value: data.amount,
      //   })
      //   .session(session)
      //   .populate('receiver')
      //   .populate('product');

      // if (rewardTransaction) {
      //   if (data.resultCode == 9000) {
      //     if (rewardTransaction.status == 'pending') {
      //       this.sendConfirmToMomo(rewardTransaction);
      //     } else this.sendCancelToMomo(rewardTransaction);
      //   } else if (data.resultCode != 0 && data.resultCode != 9000) {
      //     await rewardTransaction.updateOne(
      //       {
      //         status: 'failed',
      //         paymentInfo: { ...rewardTransaction.paymentInfo, ...data },
      //       },
      //       { session },
      //     );

      //     // setTimeout(() => {
      //     //   this.eventsGateway.onEnergyTransaction({
      //     //     productID: productID,
      //     //     status: 'fail',
      //     //     transactionID: data.orderId,
      //     //   });
      //     // }, 10000);

      //     await session.commitTransaction();
      //   }
      // }
    } catch (error) {
      const productID = data.extraData;
      // setTimeout(() => {
      //   this.eventsGateway.onEnergyTransaction({
      //     productID: productID,
      //     status: 'fail',
      //     transactionID: data.orderId,
      //   });
      // }, 10000);
      console.log('handleIpnNoti -> reward: ', error);
      Logger.error('Update reward transaction status failed', error);
    } finally {
      // await session.endSession();

      return {
        data: 'success',
      };
    }
  }

  async sendConfirmToMomo(transaction: RewardTransaction) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'capture',
        amount: transaction.value,
        lang: 'vi',
        description: `Nhận tiền thưởng bắt ${
          transaction?.product?.name || 'Pokemon'
        }`,
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });

      const responseData = await response.json();
      Logger.log('Receive response', responseData);

      const transactionID = responseData.orderId;
      const productID = String(transaction.product._id);

      if (responseData.resultCode == 0) {
        await this.rewardTransactionModel.findOneAndUpdate(
          { _id: transactionID, status: 'pending' },
          {
            status: 'success',
            statusRequestPay: 'pending',
            paymentInfo: { ...transaction.paymentInfo, ...responseData },
          },
          session,
        );

        await session.commitTransaction();

        this.sendMoneyToSeller(transaction._id.toString());

        // setTimeout(() => {
        //   this.eventsGateway.onTransaction({
        //     ticketID: productID,
        //     status: 'success',
        //     transactionID: responseData.orderId,
        //   });
        // }, 10000);
      }
    } catch (err) {
      Logger.error('Send confirm to momo failed', err);
      throw new BadRequestException(err);
    }
  }

  async sendCancelToMomo(transaction: RewardTransaction) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'cancel',
        amount: transaction.value,
        lang: 'vi',
        description: `Nhận tiền thưởng bắt ${
          transaction?.product?.name || 'Pokemon'
        }`,
        signature: '',
      };

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData = await response.json();
      Logger.log('Receive sendCancelToMomo response: ', responseData);

      const productID = String(transaction.product);

      // if (responseData.resultCode == 0) {
      //         // socket here
      //         setTimeout(() => {
      //           this.eventsGateway.onEnergyTransaction({
      //             productID: productID,
      //             status: 'failed',
      //             transactionID: responseData.orderId,
      //           });
      //         }, 10000);
      //       }

      await session.commitTransaction();
    } catch (error) {
      Logger.error('Send cancel to momo failed', error);
      throw new BadRequestException(error);
    }
  }

  async sendMoneyToSeller(transactionID: string, session?: any) {
    const transaction = await this.rewardTransactionModel
      .findById(transactionID)
      .populate('receiver')
      .populate('product')
      .session(session);

    const controller = new AbortController();
    const setTimeoutId = setTimeout(() => controller.abort(), 30000);

    try {
      const now = Date.now();
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_SEND,
        orderId: transaction._id.toString() + now,
        orderInfo: `Nhận tiền thưởng bắt ${
          transaction?.product?.name || 'Pokemon'
        }`,
        extraData: '',
        lang: 'vi',
        amount: transaction.value,
        walletId: transaction.receiver.phone,
        requestId: transaction._id.toString() + now,
        requestType: 'disburseToWallet',
        disbursementMethod: '',
        signature: '',
      };

      data.extraData = genBase64(`{"id": "${transaction._id.toString()}"}`);

      const disbursementData = {
        partnerCode: process.env.PARTNER_CODE,
        partnerRefId: transaction._id.toString() + now,
        partnerTransId: transaction._id.toString() + now,
        amount: transaction.value,
        description: `Nhận tiền thưởng bắt ${
          transaction?.product?.name || 'Pokemon'
        }`,
        walletId: transaction.receiver.phone,
      };

      data.disbursementMethod = genDisbursementMethod(
        JSON.stringify(disbursementData),
      );

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&disbursementMethod=${data.disbursementMethod}&extraData=${data.extraData}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);
      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_REQUEST, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
        signal: controller.signal,
      });
      const responseData = await response.json();

      if (setTimeoutId) clearTimeout(setTimeoutId);

      if (responseData['resultCode'] != 0) {
        await this.rewardTransactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            statusRequestPay: 'failed',
            requestPayInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        Logger.error(
          `Request payment to wallet ${transaction.receiver.phone} failed`,
          responseData,
        );
      } else {
        await this.rewardTransactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            statusRequestPay: 'success',
            requestPayInfo: { ...transaction.requestPayInfo, ...responseData },
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        Logger.log(
          `Request payment to wallet ${transaction.receiver.phone} success`,
          responseData,
        );
      }
    } catch (err) {
      if (setTimeoutId) clearTimeout(setTimeoutId);

      await this.rewardTransactionModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { statusRequestPay: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      Logger.error(
        `Request payment to wallet ${transaction.receiver.phone} error`,
        err,
      );
    } finally {
      if (setTimeoutId) clearTimeout(setTimeoutId);
    }
  }
}
