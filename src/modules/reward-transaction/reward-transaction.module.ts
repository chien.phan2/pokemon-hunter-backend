import { Module } from '@nestjs/common';
import { RewardTransactionController } from './reward-transaction.controller';
import { RewardTransactionService } from './reward-transaction.service';
import { MongooseModule } from '@nestjs/mongoose';
import {
  RewardTransaction,
  RewardTransactionSchema,
} from 'src/schema/reward-transaction.schema';
import { UserModule } from '../user/user.module';
import { PokemonModule } from '../pokemon/pokemon.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: RewardTransaction.name, schema: RewardTransactionSchema },
    ]),
    UserModule,
    PokemonModule,
  ],
  controllers: [RewardTransactionController],
  providers: [RewardTransactionService],
  exports: [RewardTransactionService],
})
export class RewardTransactionModule {}
