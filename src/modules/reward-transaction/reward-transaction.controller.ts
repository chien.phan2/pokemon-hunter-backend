import {
  Body,
  Controller,
  HttpCode,
  Logger,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { RewardTransactionService } from './reward-transaction.service';

@ApiTags('Reward Transaction')
@Controller('reward-transaction')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class RewardTransactionController {
  constructor(
    private readonly rewardTransactionService: RewardTransactionService,
  ) {}

  @Post('/ipn')
  @HttpCode(204)
  ipnNoti(@Body() body: any) {
    console.log('Receive reward IPN:', body);
    Logger.log('Receive reward IPN:', body);
    return this.rewardTransactionService.handleIpnNoti(body);
  }
}
