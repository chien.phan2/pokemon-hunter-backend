import { Test, TestingModule } from '@nestjs/testing';
import { RewardTransactionService } from './reward-transaction.service';

describe('RewardTransactionService', () => {
  let service: RewardTransactionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RewardTransactionService],
    }).compile();

    service = module.get<RewardTransactionService>(RewardTransactionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
