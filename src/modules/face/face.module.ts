import { Module } from '@nestjs/common';
import { FaceController } from './face.controller';
import { FaceService } from './face.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Face, FaceSchema } from 'src/schema/face.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Face.name, schema: FaceSchema }]),
  ],
  controllers: [FaceController],
  providers: [FaceService],
  exports: [FaceService],
})
export class FaceModule {}
