import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformHookResponseInterceptor } from 'src/interceptor/hook.interceptor';
import { FaceService } from './face.service';
import { CreateFaceDto } from './dto/create-face.dto.ts';
import { UpdateFaceDto } from './dto/update-face.dto.ts';

@ApiTags('Face')
@Controller('face')
@UseInterceptors(new TransformHookResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class FaceController {
  constructor(private readonly faceService: FaceService) {}

  @Post('/create')
  async createFace(@Body() payload: CreateFaceDto) {
    return this.faceService.createFace(payload);
  }

  @Patch('/update/:id')
  async updateFace(@Body() payload: UpdateFaceDto, @Param('id') id: string) {
    return this.faceService.updateFace(payload, id);
  }

  @Get('/get-all')
  async getAllFaces() {
    return this.faceService.getAllFaces();
  }

  @Get('/:id')
  async getFaceById(@Param('id') id: string) {
    return this.faceService.getFaceById(id);
  }
}
