import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Face, FaceDocument } from 'src/schema/face.schema';
import { Model } from 'mongoose';
import { CreateFaceDto } from './dto/create-face.dto.ts';
import { UpdateFaceDto } from './dto/update-face.dto.ts.js';

@Injectable()
export class FaceService {
  constructor(
    @InjectModel(Face.name) private readonly faceModel: Model<FaceDocument>,
  ) {}

  async createFace(payload: CreateFaceDto): Promise<Face> {
    try {
      const newFace = new this.faceModel(payload);
      return newFace.save();
    } catch (error) {
      console.log('createFace: ', error);
      throw new BadRequestException(error);
    }
  }

  async updateFace(payload: UpdateFaceDto, id: string): Promise<Face> {
    try {
      const updatedFace = await this.faceModel.findByIdAndUpdate(
        id,
        { ...payload, updatedAt: new Date() },
        { new: true },
      );
      return updatedFace;
    } catch (error) {
      console.log('updateFace: ', error);
      throw new BadRequestException(error);
    }
  }

  async getAllFaces() {
    try {
      const faces = await this.faceModel.find();
      return faces;
    } catch (error) {
      console.log('getAllFaces: ', error);
      throw new BadRequestException(error);
    }
  }

  async getFaceById(id: string): Promise<Face> {
    try {
      const face = await this.faceModel.findById(id);
      return face;
    } catch (error) {
      console.log('getFaceById: ', error);
      throw new BadRequestException(error);
    }
  }
}
