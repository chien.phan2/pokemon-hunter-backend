import {
  Controller,
  Get,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { InventoryService } from './inventory.service';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { Request } from 'express';

@ApiTags('Inventory')
@Controller('inventory')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class InventoryController {
  constructor(private readonly inventoryService: InventoryService) {}

  @Get('/')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  async getMyBalls(@Req() req: Request) {
    const userID = req.userID;
    return this.inventoryService.getInventoryItemByUserID(userID);
  }
}
