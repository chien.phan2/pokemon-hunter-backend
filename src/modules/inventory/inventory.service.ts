import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Inventory, InventoryDocument } from 'src/schema/inventory.schema';
import { Model } from 'mongoose';
import { CreateInventoryDto } from './dto/create-inventory.dto';
import { UserService } from '../user/user.service';
import { BallService } from '../ball/ball.service';

@Injectable()
export class InventoryService {
  constructor(
    @InjectModel(Inventory.name)
    private inventoryModel: Model<InventoryDocument>,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
    @Inject(forwardRef(() => BallService))
    private readonly ballService: BallService,
  ) {}

  async createInventoryItem(payload: CreateInventoryDto, userID: string) {
    try {
      const user = await this.userService.getUserInfo(userID);
      const ball = await this.ballService.getBallById(payload?.product);
      const data = {
        product: ball?._id?.toString(),
        user: user?._id?.toString(),
        isUsed: false,
      };
      const newInventoryItem = new this.inventoryModel(data);
      return await newInventoryItem.save();
    } catch (error) {
      console.log('createInventoryItem: ', error);
    }
  }

  async setInventoryItemUsed(id: string) {
    try {
      const updatedInventoryItem = await this.inventoryModel.findByIdAndUpdate(
        id,
        { $set: { isUsed: true } },
        { new: true },
      );
      return updatedInventoryItem;
    } catch (error) {
      console.log('setInventoryItemUsed: ', error);
      throw new BadRequestException(error);
    }
  }

  async restoreInventoryItem(id: string) {
    try {
      const updatedInventoryItem = await this.inventoryModel.findByIdAndUpdate(
        id,
        { $set: { isUsed: false } },
        { new: true },
      );
      return updatedInventoryItem;
    } catch (error) {
      console.log('restoreInventoryItem: ', error);
      throw new BadRequestException(error);
    }
  }

  async getInventoryItemByUserID(userID: string) {
    try {
      const inventoryItems = await this.inventoryModel
        .aggregate([
          {
            $match: {
              user: userID,
              isUsed: false,
            },
          },
          {
            $group: {
              _id: '$product',
              count: { $sum: 1 },
            },
          },
          // {
          //   $unwind: '$product',
          // },
        ])
        .exec();
      return inventoryItems;
    } catch (error) {
      console.log('getInventoryItemByUserID: ', error);
    }
  }

  async checkInventoryItemExist(userID: string, productID: string) {
    try {
      const inventoryItem = await this.inventoryModel.find({
        user: userID,
        product: productID,
        isUsed: false,
      });
      return inventoryItem;
    } catch (error) {
      console.log('checkInventoryItemExist: ', error);
      throw new BadRequestException(error);
    }
  }
}
