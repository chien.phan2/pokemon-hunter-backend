import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { RoomService } from './room.service';
import { RoomCreateDto } from './dto/room-create.dto';
import { RoomUpdateDto } from './dto/room-update.dto';
import { PortalAuthGuard } from 'src/middleware/auth.middleware';

@ApiTags('Room Management')
@Controller()
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @Post('portal/create-room')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async createRoom(@Body() room: RoomCreateDto) {
    return await this.roomService.createRoom(room);
  }

  @Get('portal/start-room/:roomId')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async startRoom(@Param('roomId') roomId: string) {
    return await this.roomService.startRoom(roomId);
  }

  @Get('portal/end-room/:roomId')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async endRoom(@Param('roomId') roomId: string) {
    return await this.roomService.endRoom(roomId);
  }

  @Patch('portal/update-room/:roomId')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async updateRoom(
    @Param('roomId') roomId: string,
    @Body() room: RoomUpdateDto,
  ) {
    return await this.roomService.updateRoom(room, roomId);
  }

  @Get('portal/rooms')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async getRooms() {
    return await this.roomService.getRooms();
  }

  @Get('portal/current-room')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async getCurrentRoom() {
    return await this.roomService.getCurrentRoom();
  }
}
