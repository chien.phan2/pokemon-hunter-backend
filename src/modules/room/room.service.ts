import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EventsGateway } from 'src/events/events.gateway';
import { Round, RoundDocument } from 'src/schema/room.schema';
import { PokemonService } from '../pokemon/pokemon.service';
import { RoomCreateDto } from './dto/room-create.dto';
import { RoomUpdateDto } from './dto/room-update.dto';

@Injectable()
export class RoomService {
  constructor(
    @InjectModel(Round.name) private roomModel: Model<RoundDocument>,
    private readonly pokemonService: PokemonService,
    private readonly eventsGateway: EventsGateway,
  ) {}

  async createRoom(room: RoomCreateDto): Promise<Round> {
    try {
      const newRoom = new this.roomModel(room);
      await newRoom.save();

      return newRoom;
    } catch (error) {
      console.log('createRoom: ', error);
      throw new BadRequestException(error);
    }
  }

  async updateRoom(payload: RoomUpdateDto, roomId: string): Promise<Round> {
    try {
      const updatedRoom = await this.roomModel
        .findByIdAndUpdate(
          roomId,
          {
            $set: {
              ...payload,
            },
          },
          { new: true },
        )
        .exec();

      return updatedRoom;
    } catch (error) {
      console.log('updateRoom: ', error);
      throw new BadRequestException(error);
    }
  }

  async startRoom(roomId: string): Promise<any> {
    try {
      const updateRoom = await this.roomModel.findByIdAndUpdate(
        roomId,
        {
          $set: {
            isActive: true,
          },
        },
        { new: true },
      );

      const pokemonsInRoom = await this.pokemonService.getPokemonsInRoom(
        updateRoom?._id?.toString(),
      );
      if (pokemonsInRoom?.pokemons?.length === 0) {
        throw new BadRequestException('No pokemons in the room');
      }

      const pokemonIds = pokemonsInRoom?.pokemons?.map((pokemon) =>
        pokemon?._id?.toString(),
      );
      const promises = [];
      pokemonIds.forEach((pokemonId) => {
        promises.push(
          this.pokemonService.updatePokemon(
            { isShow: true, isOnGame: true },
            pokemonId,
          ),
        );
      });

      const result = await Promise.all(promises);

      const currentPokemons = await this.pokemonService.getCurrentPokemon();

      setTimeout(() => {
        this.eventsGateway.onChangePokemon({
          data: currentPokemons,
        });
      }, 1000);

      return result;
    } catch (error) {
      console.log('startRoom: ', error);
      throw new BadRequestException(error);
    }
  }

  async endRoom(roomId: string): Promise<any> {
    try {
      const updateRoom = await this.roomModel.findByIdAndUpdate(
        roomId,
        {
          $set: {
            isActive: false,
          },
        },
        { new: true },
      );

      const pokemonsInRoom = await this.pokemonService.getPokemonsInRoom(
        updateRoom?._id?.toString(),
      );
      if (pokemonsInRoom?.pokemons?.length === 0) {
        throw new BadRequestException('No pokemons in the room');
      }

      const pokemonIds = pokemonsInRoom?.pokemons?.map((pokemon) =>
        pokemon?._id?.toString(),
      );
      const promises = [];
      pokemonIds.forEach((pokemonId) => {
        promises.push(
          this.pokemonService.updatePokemon(
            { isShow: false, isActive: false },
            pokemonId,
          ),
        );
      });

      const result = await Promise.all(promises);

      const currentPokemons = await this.pokemonService.getCurrentPokemon();

      setTimeout(() => {
        this.eventsGateway.onChangePokemon({
          data: currentPokemons,
        });
      }, 1000);

      return result;
    } catch (error) {
      console.log('startRoom: ', error);
      throw new BadRequestException(error);
    }
  }

  async getRooms(): Promise<Round[]> {
    try {
      const rooms = await this.roomModel
        .find({})
        .select('_id name isActive isCompleted')
        .exec();

      const roomIds = rooms.map((room) => room?._id?.toString());

      const promises = [];
      roomIds.forEach((roomId) => {
        promises.push(this.pokemonService.getPokemonsInRoom(roomId));
      });

      const result = await Promise.all(promises);

      const data = rooms.map((item) => {
        return {
          ...item?.toJSON(),
          pokemons:
            result?.find((pokemon) => pokemon?.room === item?._id?.toString())
              ?.pokemons || [],
        };
      });

      return data;
    } catch (error) {
      console.log('getRooms: ', error);
      throw new BadRequestException(error);
    }
  }

  async getCurrentRoom(): Promise<Round> {
    try {
      const room = await this.roomModel.findOne({ isActive: true }).exec();

      return room;
    } catch (error) {
      console.log('currentRoom: ', error);
      throw new BadRequestException(error);
    }
  }
}
