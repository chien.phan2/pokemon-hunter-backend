import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventsModule } from 'src/events/events.module';
import { Round, RoundSchema } from 'src/schema/room.schema';
import { PokemonModule } from '../pokemon/pokemon.module';
import { RoomController } from './room.controller';
import { RoomService } from './room.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Round.name,
        schema: RoundSchema,
      },
    ]),
    PokemonModule,
    EventsModule,
  ],
  controllers: [RoomController],
  providers: [RoomService],
  exports: [RoomService],
})
export class RoomModule {}
