import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class RoomCreateDto {
  @ApiProperty({ default: null, required: true })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({ default: null, required: false })
  @IsOptional()
  @IsString()
  description: string;
}
