import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { PortalAuthGuard } from 'src/middleware/auth.middleware';
import { ActivePokemon } from './dto/active-pokemon.dto';
import { CreatePokemonDto } from './dto/create-pokemon.dto';
import { FilterPokemonDto } from './dto/filter-pokemon.dto';
import { LeakPokemonDto } from './dto/leak-pokemon.dto';
import { PokemonBackupDto } from './dto/pokemon-backup.dto';
import { PokemonFilterDto } from './dto/pokemon-filter.dto';
import { UpdatePokemonDto } from './dto/update-pokemon.dto';
import { PokemonService } from './pokemon.service';

@ApiTags('Pokemon')
@Controller('pokemon')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class PokemonController {
  constructor(private readonly pokemonService: PokemonService) {}

  @Get('/portal/pokemons')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async getPokemons(@Query() query: PokemonFilterDto) {
    return this.pokemonService.getPokemons(query);
  }

  @Get('/portal/backup-pokemons')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async getBackupPokemons(@Query() query: PokemonFilterDto) {
    return this.pokemonService.getBackupPokemons(query);
  }

  @Get('/portal/amount-remain-pokemons')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async countAmountRemainAllPokemon() {
    return this.pokemonService.countAmountRemainAllPokemon();
  }

  @Patch('/portal/replace-pokemon')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async replacePokemon(@Body() payload: PokemonBackupDto) {
    return this.pokemonService.replacePokemon(payload);
  }

  @Post('/create')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async createPokemon(@Body() payload: CreatePokemonDto) {
    return this.pokemonService.createPokemon(payload);
  }

  @Patch('/update/:id')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new PortalAuthGuard())
  async updatePokemon(
    @Body() payload: UpdatePokemonDto,
    @Param('id') id: string,
  ) {
    return this.pokemonService.updatePokemon(payload, id);
  }

  @Get('/get-all')
  @ApiQuery({
    name: 'accessKey',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'isActive',
    required: false,
    type: Boolean,
  })
  @ApiQuery({
    name: 'rate',
    required: false,
    type: Number,
  })
  @ApiQuery({
    name: 'type',
    required: false,
    type: String,
  })
  @ApiQuery({
    name: 'level',
    required: false,
    type: Number,
  })
  async getAllPokemons(@Query() query: FilterPokemonDto) {
    return this.pokemonService.getAllPokemons(query);
  }

  @Get('/get-current-pokemon')
  async getCurrentPokemon() {
    return this.pokemonService.getCurrentPokemon();
  }

  @Patch('/public-pokemon')
  async publicPokemon(@Body() payload: ActivePokemon) {
    return this.pokemonService.publicPokemon(payload);
  }

  @Patch('/leak-pokemon')
  async leakPokemon(@Body() payload: LeakPokemonDto) {
    return this.pokemonService.leakPokemon(payload);
  }

  @Get('/:id')
  async getPokemonById(@Param('id') id: string) {
    return this.pokemonService.getPokemonById(id);
  }
}
