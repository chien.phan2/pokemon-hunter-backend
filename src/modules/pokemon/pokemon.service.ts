import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { EventsGateway } from 'src/events/events.gateway';
import { Pokemon, PokemonDocument } from 'src/schema/pokemon.schema';
import { ActivePokemon } from './dto/active-pokemon.dto.js';
import { CreatePokemonDto } from './dto/create-pokemon.dto.js';
import { FilterPokemonDto } from './dto/filter-pokemon.dto.js';
import { LeakPokemonDto } from './dto/leak-pokemon.dto.js';
import { PokemonBackupDto } from './dto/pokemon-backup.dto.js';
import { PokemonFilterDto } from './dto/pokemon-filter.dto.js';
import { UpdatePokemonDto } from './dto/update-pokemon.dto.js';

@Injectable()
export class PokemonService {
  constructor(
    @InjectModel(Pokemon.name) private pokemonModel: Model<PokemonDocument>,
    private readonly eventsGateway: EventsGateway,
    @InjectConnection() private connection: Connection,
  ) {}

  async createPokemon(pokemon: CreatePokemonDto): Promise<Pokemon> {
    try {
      const newPokemon = new this.pokemonModel(pokemon);
      return newPokemon.save();
    } catch (error) {
      console.log('createPokemon: ', error);
      throw new BadRequestException(error);
    }
  }

  async getPokemons(filter: PokemonFilterDto) {
    // const records = await this.pokemonModel.find({});
    // const bulkOps = records.map((record) => {
    //   const parts = record.name.split(' - ');
    //   const newName = parts[1]; // Get the second part after the hyphen

    //   return {
    //     updateOne: {
    //       filter: { _id: record._id },
    //       update: { $set: { name: newName } },
    //     },
    //   };
    // });

    // const result = await this.pokemonModel.bulkWrite(bulkOps);
    // console.log('====> result:', result);
    try {
      const query = {};
      if (filter?.location !== 'All') {
        query['location'] = filter?.location;
      }

      const pokemons = await this.pokemonModel.find(query).exec();
      return pokemons;
    } catch (error) {
      console.log('createPokemons: ', error);
      throw new BadRequestException(error);
    }
  }

  async getPokemonsInRoom(roomId: string): Promise<any> {
    try {
      const pokemons = await this.pokemonModel
        .find({ room: roomId })
        .select(
          '_id room name fortune budget bonusBudget location randomRange isShow',
        )
        .exec();

      return {
        room: roomId,
        pokemons: pokemons,
      };
    } catch (error) {
      console.log('getPokemonsInRoom: ', error);
      throw new BadRequestException(error);
    }
  }

  async updatePokemon(pokemon: UpdatePokemonDto, id: string): Promise<Pokemon> {
    try {
      const updatedPokemon = await this.pokemonModel.findByIdAndUpdate(
        id,
        { ...pokemon, updatedAt: new Date() },
        { new: true },
      );

      // setTimeout(() => {
      //   this.eventsGateway.onChangePokemon({
      //     pokemon: updatedPokemon?._id.toString(),
      //     status: 'success',
      //     isActive: updatedPokemon?.isActive,
      //   });
      // }, 1000);

      return updatedPokemon;
    } catch (error) {
      console.log('updatePokemon: ', error);
      throw new BadRequestException(error);
    }
  }

  async getAllPokemons(filter: FilterPokemonDto): Promise<Pokemon[]> {
    try {
      const query = {
        ...filter,
      };

      if (
        !filter?.accessKey ||
        filter?.accessKey !== process.env.ACCESS_KEY_PORTAL
      ) {
        query['isShow'] = true;
      }

      const pokemons = await this.pokemonModel.find({
        ...query,
      });

      // if (filter?.accessKey === process.env.ACCESS_KEY_PORTAL) {
      //   return pokemons;
      // }

      const pokemonList = pokemons.map((item) => {
        return {
          ...item.toJSON(),
          faceMatchingName: 'Bị lừa gòi',
          fortune: 0,
        };
      });

      return pokemonList;
    } catch (error) {
      console.log('getAllPokemon: ', error);
      throw new BadRequestException(error);
    }
  }

  async getPokemonById(id: string): Promise<Pokemon> {
    try {
      const pokemon = await this.pokemonModel.findById(id);
      return pokemon;
    } catch (error) {
      console.log('getPokemonById: ', error);
      throw new BadRequestException(error);
    }
  }

  async publicPokemon(payload: ActivePokemon): Promise<Pokemon> {
    try {
      // await this.pokemonModel.updateMany({
      //   isActive: false,
      // });

      const pokemon = await this.pokemonModel.findByIdAndUpdate(
        payload?.pokemonId,
        { isActive: payload?.isActive },
        { new: true },
      );

      // if (payload?.isActive) {
      //   setTimeout(() => {
      //     this.eventsGateway.onChangePokemon({
      //       pokemon: pokemon?._id.toString(),
      //       status: 'success',
      //       isActive: true,
      //       isLeak: pokemon?.isLeak || false,
      //     });
      //   }, 5000);
      // }

      return pokemon;
    } catch (error) {
      console.log('publicPokemon: ', error);
      throw new BadRequestException(error);
    }
  }

  async leakPokemon(payload: LeakPokemonDto): Promise<Pokemon> {
    try {
      const pokemon = await this.pokemonModel.findByIdAndUpdate(
        payload?.pokemonId,
        { isLeak: true },
        { new: true },
      );

      setTimeout(() => {
        this.eventsGateway.onChangePokemon({
          pokemon: pokemon?._id.toString(),
          status: 'success',
          isActive: pokemon?.isActive,
          isLeak: true,
        });
      }, 5000);

      return pokemon;
    } catch (error) {
      console.log('leakPokemon: ', error);
      throw new BadRequestException(error);
    }
  }

  async getCurrentPokemon() {
    try {
      const pokemons = await this.pokemonModel
        .find({
          isShow: true,
        })
        .select(
          '-rate -level -budgetSet -budgetSlot -bonusBudgetSet -createdAt -updatedAt -__v -randomRange -isActive -isShow -isCompleted -isOnGame -room -bonusBudgetSlot',
        )
        .exec();

      return pokemons;
    } catch (error) {
      console.log('getCurrentPokemon: ', error);
      throw new BadRequestException(error);
    }
  }

  async calculateBudgetPokemon(pokemonId) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const pokemon = await this.pokemonModel.findById(pokemonId);
      if (!pokemon) {
        throw new BadRequestException('Không tìm thấy pokemon');
      }

      const budget = pokemon?.budget || 0;
      // const budgetSet = pokemon?.budgetSet || 0;
      // const budgetSlot = pokemon?.budgetSlot || 1;

      const bonusBudget = pokemon?.bonusBudget || 0;
      const bonusBudgetSet = pokemon?.bonusBudgetSet || 0;
      const bonusBudgetSlot = pokemon?.bonusBudgetSlot || 1;

      const avgBonusBudget = bonusBudgetSet / bonusBudgetSlot;

      const randomRange = pokemon?.randomRange || [15000, 25000];
      const min = randomRange?.[0];
      const max = randomRange?.[1];

      // Calculate the number of possible steps within the range
      const numberOfSteps = Math.floor((max - min) / 1000) + 1;

      // Generate a random step index
      const randomStepIndex = Math.floor(Math.random() * numberOfSteps);

      // Calculate the random number using the selected step index
      const randomNumber = min + randomStepIndex * 1000;

      // const avgBudget = budgetSet / budgetSlot;
      // const randomValue = Number((0.5 + Math.random() * 1).toFixed(2));

      // const receiveBudget = Number((avgBudget * randomValue).toFixed(2));
      const receiveBudget = randomNumber;
      const receiveBonusBudget = avgBonusBudget;

      let realReceiveBudget = Number(receiveBudget.toFixed(2));
      let realReceiveBonusBudget = Number(receiveBonusBudget.toFixed(2));
      let status = 'success';

      if (bonusBudget - receiveBonusBudget < 0) {
        realReceiveBonusBudget = 0;
      }

      if (budget - receiveBudget < 0) {
        realReceiveBudget = 0;
        status = 'failed';
      }

      await pokemon.updateOne(
        {
          budget: budget - realReceiveBudget,
          bonusBudget: bonusBudget - realReceiveBonusBudget,
        },
        {
          session,
        },
      );

      setTimeout(() => {
        this.eventsGateway.onChangeBudget({
          pokemon: pokemonId,
          remainBudget:
            budget - realReceiveBudget + (bonusBudget - realReceiveBonusBudget),
        });
      }, 2000);

      await session.commitTransaction();
      await session.endSession();

      return {
        reward: realReceiveBudget,
        bonusReward: realReceiveBonusBudget,
        totalReward: realReceiveBudget + realReceiveBonusBudget,
        status: status,
      };
    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      console.log('calculateBudgetPokemon: ', error);
      throw new BadRequestException(error);
    }
  }

  async getBackupPokemons(payload: PokemonFilterDto) {
    try {
      const pokemons = await this.pokemonModel
        .find({
          location: payload?.location,
          isActive: true,
          $or: [{ room: null }, { room: { $exists: false } }, { room: '' }],
        })
        .select(
          '_id room name fortune budget bonusBudget location randomRange isShow',
        )
        .exec();

      return pokemons;
    } catch (error) {
      console.log('getBackupPokemons: ', error);
      throw new BadRequestException(error);
    }
  }

  async replacePokemon(payload: PokemonBackupDto) {
    try {
      const hideCurrentPokemon = await this.pokemonModel.findByIdAndUpdate(
        payload?.currentPokemon,
        { isShow: false, isActive: false, isOnGame: false },
        { new: true },
      );

      const updatePokemon = await this.updatePokemon(
        { room: payload?.room, isShow: true, isOnGame: true },
        payload?.newPokemon,
      );

      const currentPokemons = await this.getCurrentPokemon();

      setTimeout(() => {
        this.eventsGateway.onChangePokemon({
          data: currentPokemons,
        });
      }, 1000);

      return {
        updatePokemon,
        hideCurrentPokemon,
      };
    } catch (error) {
      console.log('replacePokemon: ', error);
      throw new BadRequestException(error);
    }
  }

  async countAmountRemainAllPokemon() {
    try {
      const pokemons = await this.pokemonModel
        .find({
          isOnGame: true,
        })
        .exec();
      let totalBudget = 0;
      let totalBonusBudget = 0;

      pokemons.forEach((pokemon) => {
        totalBudget += pokemon?.budget || 0;
        totalBonusBudget += pokemon?.bonusBudget || 0;
      });

      return {
        totalBudget,
        totalBonusBudget,
      };
    } catch (error) {
      console.log('countBudgetAndBonusBudgetAllPokemon: ', error);
      throw new BadRequestException(error);
    }
  }
}
