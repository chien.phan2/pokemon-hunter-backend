export class PublicPokemon {
  _id: any;
  name: string;
  description: string;
  imgUrl: string;
  type: string;
  level: string;
  rate: string;
  budget: string;

  constructor(partial: any) {
    Object.assign(this, partial);
    this._id = partial._id.toString();
    this.name = partial?.name || '';
    this.description = partial?.description || '';
    this.imgUrl = partial?.imgUrl || '';
    this.type = partial?.type || '';
    this.level = partial?.level || (partial.level as unknown as number);
    this.rate = partial?.rate || (partial.owner as unknown as number);
    this.budget = partial?.budget || (partial.budget as unknown as number);
  }
}
