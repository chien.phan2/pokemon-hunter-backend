import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUrl,
  Min,
} from 'class-validator';

export class CreatePokemonDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsOptional()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsUrl()
  imgUrl: string;

  @ApiProperty()
  @IsOptional()
  faceMatchingName: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  fortune: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  budget: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  budgetSet: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  budgetSlot: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  bonusBudget: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  bonusBudgetSet: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  bonusBudgetSlot: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  location: string;
}
