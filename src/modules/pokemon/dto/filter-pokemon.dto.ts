import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class FilterPokemonDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  type: string;

  @ApiProperty()
  @IsOptional()
  rate: number;

  @ApiProperty()
  @IsOptional()
  level: number;

  @ApiProperty()
  @IsOptional()
  accessKey: string;
}
