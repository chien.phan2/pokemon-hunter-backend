import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class LeakPokemonDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  pokemonId: string;
}
