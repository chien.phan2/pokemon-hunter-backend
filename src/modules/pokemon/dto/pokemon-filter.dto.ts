import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class PokemonFilterDto {
  @ApiProperty({ enum: ['All', 'F7', 'F11', 'F12', 'F8'] })
  @IsNotEmpty()
  @IsString()
  location: string;
}
