import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class PokemonBackupDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsString()
  newPokemon: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsString()
  currentPokemon: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsString()
  room: string;
}
