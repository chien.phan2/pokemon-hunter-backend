import { Controller, Post, Body, UseInterceptors, Get } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('User')
@Controller('user')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/sync')
  sync(@Body() createUserDto: CreateUserDto) {
    return this.userService.sync(createUserDto);
  }

  @Get('/:id')
  getUserById(@Body() id: string) {
    return this.userService.getUserInfo(id);
  }
}
