import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsPhoneNumber, IsString, Matches } from 'class-validator';

export class CreateUserDto {
  @IsString()
  @ApiProperty()
  userID: string;

  @IsOptional()
  @Matches(/^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9|7]|9[0-4|6-9])[0-9]{7}$/)
  @ApiProperty()
  phone: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  name: string;
}
