import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { MY_MESSAGE } from 'src/contants/message.contants';
import { EventsGateway } from 'src/events/events.gateway';
import { User } from '../../schema/user.schema';
import { BallService } from '../ball/ball.service';
import { InventoryService } from '../inventory/inventory.service';
import { CreateUserDto } from './dto/create-user.dto';
import { PublicUser } from './dto/public-user.dto';
@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
    private readonly ballService: BallService,
    private readonly inventoryService: InventoryService,
    private readonly eventsGateway: EventsGateway,
  ) {}
  async sync(createUserDto: CreateUserDto) {
    const user = await this.userModel.findOne({ phone: createUserDto.phone });
    if (user) {
      let updatedData = user;
      if (!user.phone || !user.name) {
        updatedData = await this.userModel.findOneAndUpdate(
          {
            _id: user.id,
          },
          { ...createUserDto },
          { returnOriginal: false },
        );
      }
      return {
        data: new PublicUser(updatedData?.toJSON()),
        message: MY_MESSAGE.SYNC_USER,
      };
    }

    const newUser = await this.userModel.create({
      ...createUserDto,
      isBlocked: false,
    });
    await newUser.save();

    // Distribute pokeball to first user
    const createPromises = [];
    const productList = await this.ballService.getAllBalls('product');
    const productIdslist = productList?.map((product) =>
      product?._id?.toString(),
    );
    for (let j = 0; j < 2; j++) {
      for (let i = 0; i < 3; i++) {
        createPromises.push(
          this.inventoryService.createInventoryItem(
            { product: productIdslist[i] },
            newUser?._id?.toString(),
          ),
        );
      }
    }
    await Promise.all(createPromises);

    // const inventoryRemain =
    //   await this.inventoryService.getInventoryItemByUserID(
    //     newUser?._id?.toString(),
    //   );

    // setTimeout(() => {
    //   this.eventsGateway.onChangePokeball({
    //     userID: newUser?._id?.toString(),
    //     inventory: inventoryRemain,
    //   });
    // }, 1000);

    return {
      data: new PublicUser(newUser.toJSON()),
      message: MY_MESSAGE.CREATE_USER,
    };
  }

  async getUserInfo(id: string) {
    const user = await this.userModel.findOne({
      _id: id,
      isBlocked: false,
    });
    if (!user || !user.phone)
      throw new BadRequestException(MY_MESSAGE.INVALID_USER);
    return user;
  }

  async getAllUser() {
    const users = await this.userModel.find({
      isBlocked: false,
    });
    return users;
  }

  async updatePointOfUser(userID: string, point: number) {
    try {
      const user = await this.userModel.findByIdAndUpdate(
        userID,
        { $inc: { point: point } },
        { new: true },
      );

      if (!user) throw new BadRequestException(MY_MESSAGE.INVALID_USER);

      return user;
    } catch (error) {
      console.log('updatePointOfUser', error);
      throw new BadRequestException(error);
    }
  }
}
