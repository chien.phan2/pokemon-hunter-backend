import {
  Logger,
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { WinstonModule } from 'nest-winston';
import * as path from 'path';
import * as winston from 'winston';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EventsModule } from './events/events.module';
import { CheckAvailableMiddleware } from './middleware/available.middleware';
import { BallTransactionModule } from './modules/ball-transaction/ball-transaction.module';
import { BallModule } from './modules/ball/ball.module';
import { CatchHistoryModule } from './modules/catch-history/catch-history.module';
import { FaceModule } from './modules/face/face.module';
import { InventoryModule } from './modules/inventory/inventory.module';
import { PokemonModule } from './modules/pokemon/pokemon.module';
import { RewardTransactionModule } from './modules/reward-transaction/reward-transaction.module';
import { RoomModule } from './modules/room/room.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: '.env', isGlobal: true }),
    MongooseModule.forRootAsync({
      imports: [EventsModule, ConfigModule],
      useFactory: async (configService: ConfigService) => {
        return {
          uri: process.env.MONGO_URI,
          useNewUrlParser: true,
          useUnifiedTopology: true,
        };
      },
      inject: [ConfigService],
    }),
    WinstonModule.forRoot({
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json(),
      ),
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple(),
          ),
        }),
        new winston.transports.File({
          dirname: path.join(__dirname, '../log/'),
          filename: 'debug.log',
          level: 'debug',
        }),
        new winston.transports.File({
          dirname: path.join(__dirname, '../log/'),
          filename: 'info.log',
          level: 'info',
        }),
      ],
    }),
    BallModule,
    BallTransactionModule,
    FaceModule,
    InventoryModule,
    PokemonModule,
    RewardTransactionModule,
    UserModule,
    CatchHistoryModule,
    RoomModule,
  ],
  controllers: [AppController],
  providers: [AppService, Logger],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(CheckAvailableMiddleware)
      .forRoutes({ path: 'transaction', method: RequestMethod.POST });
  }
}
