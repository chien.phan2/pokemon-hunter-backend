import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { getToken } from 'src/utils/genToken';

@Injectable()
export class AuthGuard implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const [canAccess, userID] = await this.validateRequest(request);
    if (canAccess) {
      request.userID = userID;
    }
    return canAccess;
  }

  async validateRequest(
    request: any,
  ): Promise<[canAccess: boolean, userID: string]> {
    try {
      const header = request?.headers?.authorization || '';
      const token = header?.split(' ')[1] || '';
      if (!token) throw new Error('unAuthorization');
      if (token) {
        const userID = getToken(token);
        return [true, userID];
      }
    } catch (err) {
      console.log(err);
      return [false, null];
    }
  }
}

export class PortalAuthGuard implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const [canAccess] = await this.validateRequest(request);
    return canAccess;
  }

  async validateRequest(request: any): Promise<[canAccess: boolean]> {
    try {
      const header = request?.headers?.authorization || '';
      const token = header?.split(' ')[1] || '';
      if (!token) throw new UnauthorizedException();
      if (token === process.env.API_KEY) {
        return [true];
      }
    } catch (err) {
      console.log(err);
      return [false];
    }
  }
}
