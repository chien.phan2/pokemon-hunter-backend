import {
  BadRequestException,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import checkAvailableTime from 'src/utils/checkAvailableTime';

@Injectable()
export class CheckAvailableMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    if (!checkAvailableTime())
      throw new BadRequestException('Server is not available after 13h30');
    next();
  }
}
