import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { User } from './user.schema';
import mongoose from 'mongoose';
import { Ball } from './ball.schema';

export type BallTransactionDocument = BallTransaction & Document;

@Schema({ timestamps: true })
export class BallTransaction {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'User' })
  creator: User;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Ball' })
  product: Ball;

  @Prop({ default: 'pending' })
  status: 'pending' | 'failed' | 'success' | 'error' | 'refund';

  @Prop({ default: 0 })
  quantity: number;

  @Prop({ required: true, min: 1000 })
  value: number;

  @Prop({ default: 'buy' })
  type: string;

  @Prop()
  signature: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  paymentInfo: any;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  cashOutInfo: any;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const BallTransactionSchema =
  SchemaFactory.createForClass(BallTransaction);
