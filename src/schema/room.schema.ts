import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

export type RoundDocument = Round & Document;

@Schema({ timestamps: true })
export class Round {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: false, default: null })
  name: string;

  @Prop({ required: false, default: null })
  description: string;

  @Prop({ default: false })
  isActive: boolean;

  @Prop({ default: false })
  isCompleted: boolean;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const RoundSchema = SchemaFactory.createForClass(Round);
