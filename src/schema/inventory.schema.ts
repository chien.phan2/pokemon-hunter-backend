import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Ball } from './ball.schema';
import { User } from './user.schema';

export type InventoryDocument = Inventory & Document;

@Schema({ timestamps: true })
export class Inventory {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true, type: mongoose.Schema.Types.String, ref: 'Ball' })
  product: Ball;

  @Prop({ required: true, type: mongoose.Schema.Types.String, ref: 'User' })
  user: User;

  @Prop({ required: true, default: false })
  isUsed: boolean;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const InventorySchema = SchemaFactory.createForClass(Inventory);
