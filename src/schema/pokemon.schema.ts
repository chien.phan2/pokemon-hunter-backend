import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Round } from './room.schema';

export type PokemonDocument = Pokemon & mongoose.Document;

@Schema({ timestamps: true })
export class Pokemon {
  _id: mongoose.Types.ObjectId;

  @Prop({
    required: true,
    type: mongoose.Schema.Types.String,
    ref: Round.name,
  })
  room: Round;

  @Prop({ required: true })
  name: string;

  @Prop()
  description: string;

  @Prop({ required: true })
  imgUrl: string;

  @Prop()
  faceMatching?: string;

  @Prop()
  faceMatchingName?: string;

  @Prop({ required: true })
  fortune: number;

  @Prop()
  type: string;

  @Prop({ default: 1 })
  level: number;

  @Prop({ default: 0 })
  rate: number;

  @Prop({ required: false, default: 0 })
  budget: number;

  @Prop({ required: false, default: 1 })
  budgetSet: number;

  @Prop({ required: false, default: 1 })
  budgetSlot: number;

  @Prop({ required: true, default: 0 })
  bonusBudget: number;

  @Prop({ required: true, default: 1 })
  bonusBudgetSet: number;

  @Prop({ required: true, default: 1 })
  bonusBudgetSlot: number;

  @Prop({ default: 'Chưa tìm thấy' })
  location: string;

  @Prop({ default: [0, 0] })
  randomRange: number[];

  @Prop({ default: false })
  isActive: boolean;

  @Prop({ default: false })
  isOnGame: boolean;

  @Prop({ default: false })
  isShow: boolean;

  @Prop({ default: false })
  isCompleted: boolean;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const PokemonSchema = SchemaFactory.createForClass(Pokemon);
