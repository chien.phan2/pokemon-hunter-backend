import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

export type FaceDocument = Face & Document;

@Schema({ timestamps: true })
export class Face {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true })
  name: string;

  @Prop()
  description: string;

  @Prop({ required: true })
  imgUrl: string;

  @Prop()
  type: string;

  @Prop()
  level: number;

  @Prop({ default: false })
  isDisabled: boolean;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const FaceSchema = SchemaFactory.createForClass(Face);
