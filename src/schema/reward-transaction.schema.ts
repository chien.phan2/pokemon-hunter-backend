import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { User } from './user.schema';
import { Pokemon } from './pokemon.schema';

export type RewardTransactionDocument = RewardTransaction & Document;

@Schema({ timestamps: true })
export class RewardTransaction {
  _id: mongoose.Types.ObjectId;

  @Prop({ default: 'pending' })
  status: 'pending' | 'failed' | 'success' | 'error' | 'refund';

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Pokemon' })
  product: Pokemon;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'User' })
  receiver: User;

  @Prop({ min: 1000 })
  value: number;

  @Prop()
  signature: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  paymentInfo: any;

  @Prop({ required: 'pending' })
  statusRequestPay: 'pending' | 'failed' | 'success' | 'error';

  @Prop({ type: mongoose.Schema.Types.Mixed })
  requestPayInfo: any;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const RewardTransactionSchema =
  SchemaFactory.createForClass(RewardTransaction);
