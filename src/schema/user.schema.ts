import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  _id: mongoose.Types.ObjectId;

  @Prop()
  userID: string;

  @Prop()
  phone: string;

  @Prop()
  name: string;

  @Prop({ default: 0, min: 0 })
  point: number;

  @Prop({ default: false })
  isBlocked: boolean;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
