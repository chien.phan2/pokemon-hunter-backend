import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

export type BallDocument = Ball & Document;

@Schema({ timestamps: true })
export class Ball {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true })
  name: string;

  @Prop()
  description: string;

  @Prop({ required: true })
  imgUrl: string;

  @Prop({ required: true, default: 0 })
  rate: number;

  @Prop()
  type: string;

  @Prop({ required: true, min: 1000 })
  price: number;

  @Prop({ default: false })
  isDisabled: boolean;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const BallSchema = SchemaFactory.createForClass(Ball);
