import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Pokemon } from './pokemon.schema';
import { Ball } from './ball.schema';
import { User } from './user.schema';
import mongoose from 'mongoose';

export type CatchHistoryDocument = CatchHistory & Document;

@Schema({ timestamps: true })
export class CatchHistory {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'User' })
  creator: User;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Ball' })
  product: Ball;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Pokemon' })
  pokemon: Pokemon;

  @Prop({ default: 'pending' })
  status: 'pending' | 'failed' | 'success' | 'error';

  @Prop({ default: 0 })
  reward: number;

  @Prop({ default: null, type: mongoose.Schema.Types.Mixed })
  rewardInfo: any;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;
}

export const CatchHistorySchema = SchemaFactory.createForClass(CatchHistory);
