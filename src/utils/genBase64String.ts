import * as Base64 from 'crypto-js/enc-base64';
import * as Utf8 from 'crypto-js/enc-utf8';

export default function genBase64(data: any): string {
  const wordEncoded = Utf8.parse(data);
  const signature = Base64.stringify(wordEncoded);

  return signature;
}

export function decodeBase64(data: any): string {
  const wordEncoded = Base64.parse(data);
  const result = wordEncoded.toString(Utf8);

  return result;
}
