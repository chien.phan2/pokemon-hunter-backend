export default function getDateString(): string {
  return new Date().toLocaleDateString();
}
