import { BadRequestException } from '@nestjs/common';
import * as process from 'process';
import { MY_MESSAGE } from '../contants/message.contants';

export enum RewardType {
  CHALLENGE = 'Challenge',
  MONEY = 'Money',
  NO_REWARD = 'No reward',
}

export enum RewardCode {
  Challenge_Ticket = 'ChallengeTicket',
  Good_Luck = 'GoodLuck',
  Energy_100 = 'Energy100',
}

export const RewardList: { code: string; id: string }[] = [
  {
    code: RewardCode.Challenge_Ticket,
    id: `${RewardCode.Challenge_Ticket}_0`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_1`,
  },
  {
    code: RewardCode.Energy_100,
    id: `${RewardCode.Energy_100}_2`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_3`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_4`,
  },
  {
    code: RewardCode.Energy_100,
    id: `${RewardCode.Energy_100}_5`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_6`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_7`,
  },
];

export const authenticatedPortal = (secretKey: string) => {
  const SECRET_PORTAL_KEY = process.env.PORTAL_SECRET_KEY;
  if (secretKey !== SECRET_PORTAL_KEY) {
    throw new BadRequestException(MY_MESSAGE.AUTHENTICATE_PORTAL);
  }
};

export enum ActionType {
  GIVE_AWAY = 'give-away',
  SPONSOR = 'sponsor',
  BUY = 'buy',
  MARKET_BUY = 'market-buy',
}
